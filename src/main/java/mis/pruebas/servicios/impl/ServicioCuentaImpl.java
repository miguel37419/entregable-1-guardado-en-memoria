package mis.pruebas.servicios.impl;

import mis.pruebas.modelos.Cliente;
import mis.pruebas.modelos.Cuenta;
import mis.pruebas.servicios.ObjetoNoEncontrado;
import mis.pruebas.servicios.ServicioCliente;
import mis.pruebas.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String,Cuenta> cuentas = new ConcurrentHashMap<String,Cuenta>();
    @Autowired
    ServicioCliente servicioCliente;

    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(this.cuentas.values());
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
    this.cuentas.put(cuenta.numero,cuenta) ;
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        if(!this.cuentas.containsKey(numero))
            throw new ObjetoNoEncontrado("No existe la cuenta: " + numero);
        return this.cuentas.get(numero);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if(!this.cuentas.containsKey(cuenta.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta: " + cuenta.numero);
    this.cuentas.replace(cuenta.numero, cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {

        final Cuenta existente = this.cuentas.get(parche.numero);

        if(parche.moneda != null)
            existente.moneda = parche.moneda;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if (parche.saldo != null)
            existente.saldo = parche.saldo;

        if (parche.oficina != null)
            existente.oficina = parche.oficina;

        if (parche.tipo != null)
            existente.tipo = parche.tipo;

    this.cuentas.replace(existente.numero, existente);


    }

    @Override
    public void borrarCuenta(String numero) {
        if(!this.cuentas.containsKey(numero))
            throw new ObjetoNoEncontrado("No existe la cuenta: " + numero);
        this.cuentas.remove(numero);
    }

    @Override
    public void emparcharCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Cuenta cuentaHaReemplazar = obtenerCuenta(cuenta.numero);
        if (!cliente.codigosCuentas.contains(cuenta.numero))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");
        emparcharCuenta(cuenta);
    }

    @Override
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Cuenta cuentaHaReemplazar = obtenerCuenta(cuenta.numero);
        if (!cliente.codigosCuentas.contains(cuenta.numero))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");

        guardarCuenta(cuenta);
    }

    @Override
    public void eliminarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final Cuenta cuenta = obtenerCuenta(numeroCuenta);
        if(!cliente.codigosCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");

        cuenta.estado = "INACTIVA";
        cliente.codigosCuentas.remove(numeroCuenta);

    }

    @Override
    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        if (!cliente.codigosCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("La cuenta no esta asociada al cliente");

        final Cuenta cuenta = obtenerCuenta(numeroCuenta);
        return  cuenta;
    }


}
