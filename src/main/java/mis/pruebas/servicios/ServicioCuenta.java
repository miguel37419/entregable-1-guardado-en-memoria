package mis.pruebas.servicios;

import mis.pruebas.modelos.Cuenta;
import mis.pruebas.modelos.Cuenta;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ServicioCuenta {

    //LISTAR
    public List<Cuenta> obtenerCuentas();

    //CREAR
    public void insertarCuentaNueva(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String numero);

    //UPDATE
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    //DELETE
    public  void borrarCuenta(String numero);

    public void emparcharCuentaCliente(String documento, Cuenta cuenta);
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta);
    public void eliminarCuentaCliente( String documento, String numeroCuenta);
    public Cuenta obtenerCuentaCliente( String documento, String numeroCuenta);
}
