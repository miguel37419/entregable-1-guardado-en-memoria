package mis.pruebas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntregableV2Application {

	public static void main(String[] args) {
		SpringApplication.run(EntregableV2Application.class, args);
	}

}
