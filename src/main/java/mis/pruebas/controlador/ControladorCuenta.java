package mis.pruebas.controlador;


import mis.pruebas.modelos.Cuenta;
import mis.pruebas.servicios.ObjetoNoEncontrado;
import mis.pruebas.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;



@RestController
@RequestMapping(value = Rutas.CUENTAS)
@CrossOrigin(origins = {"http://localhost:9000/","http://localhost:63342/"})
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // http://localhost:9000/api/v1/cuentas
    @GetMapping
    public List<Cuenta> obtenerCuentas(){ return this.servicioCuenta.obtenerCuentas();}

    // http://localhost:9000/api/v1/cuentas + DATOS
    @PostMapping
    public ResponseEntity agregarCuenta(@RequestBody Cuenta cuenta){
        try {
            this.servicioCuenta.insertarCuentaNueva(cuenta);
            final var representacionMetodo = methodOn(ControladorCuenta.class).obtenerunaCuenta(cuenta.numero);
            final var urlLink = linkTo(representacionMetodo).toUri();
            return ResponseEntity.ok()
                    .location(urlLink)
                    .build();
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    // http://localhost:9000/api/v1/cuentas/{documento}
    // http://localhost:9000/api/v1/cuentas/12345678
    @GetMapping("/{numero}")
    public EntityModel<Cuenta> obtenerunaCuenta(@PathVariable String numero) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerunaCuenta(numero)).withSelfRel();
            final Link cuentas = linkTo(methodOn(this.getClass()).obtenerCuentas()).withRel(" Listar cuentas: ");
            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(numero);
            return EntityModel.of(cuenta).add(self, cuentas);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    // http://localhost:9000/api/v1/cuentas/{documento} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PutMapping("/{numero}")
    public void reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.guardarCuenta(cuenta);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    // http://localhost:9000/api/v1/cuentas/{documento} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PatchMapping("/{numero}")
    public void emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.emparcharCuenta(cuenta);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    // DELETE http://localhost:9000/api/v1/cuentas/{numero}
    // DELETE http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, x.getMessage());

        }
    }

}
