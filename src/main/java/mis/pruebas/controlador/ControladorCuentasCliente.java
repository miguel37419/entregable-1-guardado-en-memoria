package mis.pruebas.controlador;

import mis.pruebas.modelos.Cliente;
import mis.pruebas.modelos.Cuenta;
import mis.pruebas.servicios.*;
import mis.pruebas.servicios.ServicioCliente;
import mis.pruebas.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    // CRUD - GET *,GET,POST,PUT,PATCH

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioCuenta servicioCuenta;

    public static class DatosEntradaCuenta {
        public String codigoCuenta;
    };

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS -> agregarCuentaCliente(documento,cuenta)
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody DatosEntradaCuenta datosEntradaCuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, datosEntradaCuenta.codigoCuenta);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.ok().build();
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<List<String>> obtenerCuentasCliente(@PathVariable String documento) {
        try {
            final var cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch(ObjetoNoEncontrado x) {
            return ResponseEntity.notFound().build();
        }
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> obtenerCuentaCliente(documento,numeroCuenta)
    @GetMapping("/{numeroCuenta}")
    public ResponseEntity<Cuenta> obtenerCuentaCliente(@PathVariable String documento, @PathVariable String numeroCuenta) {
        try {
            final Cuenta cuenta= this.servicioCuenta.obtenerCuentaCliente(documento,numeroCuenta);
            return ResponseEntity.ok(cuenta);

        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }


    // DELETE http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> eliminarCuentaCliente(documento,numeroCuenta)
    @DeleteMapping("/{numeroCuenta}")
    public ResponseEntity eliminarCuentaCliente(@PathVariable String documento, @PathVariable String numeroCuenta) {
        try {
            this.servicioCuenta.eliminarCuentaCliente(documento,numeroCuenta);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public void reemplazarCuentaCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cuenta cuenta) {
        try {
            this.servicioCuenta.reemplazarCuentaCliente(nroDocumento, cuenta);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    @PatchMapping
    public void emparcharCuentaCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cuenta cuenta) {
        try {
            this.servicioCuenta.emparcharCuentaCliente(nroDocumento, cuenta);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

}
